import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 * Viited:
 * https://github.com/williamfiset/Algorithms/blob/master/com/williamfiset/algorithms/graphtheory/EulerianPathDirectedEdgesAdjacencyList.java
 */
public class GraphTask {

    /** Main method. */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /** Actual main method to run examples and everything. */
    public void run() {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(2000, 2000);
//        Graph myGraph = new Graph("myGraph");
//        Vertex one = myGraph.createVertex("1");
//        Vertex two = myGraph.createVertex("2");
//        myGraph.createArc("1 -> 2", one, two);
//        myGraph.createArc("2 -> 1", two, one);
//        myGraph.createArc("1 -> 2", one, two);
//        myGraph.createArc("2 -> 1", two, one);
//                Vertex three = myGraph.createVertex("3");
//                Vertex four = myGraph.createVertex("4");
//                myGraph.createArc("1 -> 4", one, four);
//                myGraph.createArc("2 -> 1", two, one);
//                myGraph.createArc("1 -> 3", one, three);
//                myGraph.createArc("2 -> 4", two, four);
//                myGraph.createArc("3 -> 2", three, two);
//                myGraph.createArc("3 -> 1", three, one);
//                myGraph.createArc("4 -> 2", four, two);
//                myGraph.createArc("4 -> 3", four, three);
        EulerianPathFinder pathFinder = new EulerianPathFinder(g);
        System.out.println(pathFinder.findEulerianPath());
        //        g.createRandomSimpleGraph(4, 3);
        //        EulerianPathFinder pathFinder = new EulerianPathFinder(g);
        //        System.out.println(g.toString());
        //        System.out.println("==============================");
        //        System.out.println(pathFinder.findEulerianPath());

    }

    /**
     * Class that represents vertex of a graph.
     * outArcStack represents the stack of out Arcs
     * inDegree - in degree of a Vertex
     * outDegree - out degree of a Vertex
     */
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        private int inDegree = 0;
        private int outDegree = 0;
        private Stack<Arc> outArcStack = new Stack<>();
        // You can add more fields, if needed

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

    }

    /** Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     *
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;
        // You can add more fields, if needed

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

    }

    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.outDegree++;
            from.first = res;
            res.target = to;
            to.inDegree++;
            from.outArcStack.add(res);
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

    }

    /**
     * Reprsesents a class that finds eulerian path of a given directed graph
     * size - amount of arcs in a graph
     * graph - graph passed to a constructor
     * vertexList - list of graph vertexes
     * eulerianPath - eulerianPath of a graph
     */

    public class EulerianPathFinder {
        private int size = 0;
        private Graph graph;
        private List<Vertex> vertexList = new ArrayList<>();
        private LinkedList<Arc> eulerianPath = new LinkedList<>();

        /**
         * Constructor that inits initial data and checks graph
         * @param graph - graph passed and used for finding the eulerian path
         */
        public EulerianPathFinder(Graph graph) {
            this.graph = graph;
            init();
            checkGraph();
        }

        /**
         * This method finds the eulerian path
         * @return returns eulerian path as a linked list.
         */
        public LinkedList<Arc> findEulerianPath() {
            long start = System.currentTimeMillis();
            performDepthFirstSearch(findStartNode());
            long finish = System.currentTimeMillis() - start;
            System.out.println("Time elapsed: " + finish + "ms");
            System.out.println("Euler path size: " + eulerianPath.size());
            return eulerianPath;
        }

        /**
         * Recusively performs depth first search through a graph
         * @param vertex - vertex passed for search
         */
        private void performDepthFirstSearch(Vertex vertex) {
            while (vertex.outDegree != 0) {
                vertex.outDegree--;
                Arc arc = vertex.outArcStack.pop();
                eulerianPath.add(arc);
                Vertex next = arc.target;
                performDepthFirstSearch(next);
            }
        }

        /**
         * Finds node to start from and is used to trigger depth first search
         * @return
         */
        private Vertex findStartNode() {
            Vertex start = vertexList.get(0);
            for (int i = 0; i < vertexList.size(); i++) {
                // Unique starting node.
                Vertex currentVertex = vertexList.get(i);
                if (currentVertex.outDegree - currentVertex.inDegree == 1) {
                    return currentVertex;
                }

                // Start at a node with an outgoing edge.
                if (currentVertex.outDegree > 0) {
                    start = currentVertex;
                }
            }
            return start;
        }

        /**
         * Checks graph according to rules
         */
        private void checkGraph() {
            for (Vertex ver : vertexList) {
                if (ver.inDegree == 0 && ver.outDegree == 0) {
                    throw new RuntimeException("The graph should be connected to find Eulerian Path! " + ver.id + " is not connected");
                }
                if (ver.inDegree != ver.outDegree) {
                    throw new RuntimeException("Each vertex should have same amount of in and outgoing edges");
                }
            }
        }

        /**
         * Initializes data for further usage
         */
        private void init() {
         /*
         Here we initiate the list of all vertexes in a graph
          */
            Vertex currentVertex = graph.first;
            vertexList.add(currentVertex);
            while (currentVertex.next != null) {
                vertexList.add(currentVertex.next);
                currentVertex = currentVertex.next;
            }
            /*
            Here we calculate number of arcs in a graph
             */
            for (Vertex vertex : vertexList) {
                size += vertex.outDegree;
            }
            System.out.println("Graph size(number of arcs) = " + size);
        }
    }

} 

